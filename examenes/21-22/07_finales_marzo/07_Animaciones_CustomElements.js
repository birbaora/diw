
class MiBoton extends HTMLElement {
   veces = 1;
	vecesIniciales = 1;
   color="white";
   textoInicial="";
   textoFinal="";
   divInterno;
	tiempo=1000;
   intervalo;

creaEstiloBoton(color) {
	return `
	.boton-base {
	 width: 100%;
	  margin-left: auto;
	  margin-right: auto;
	  display: flex;
	  flex-direction: column;
	  align-items: center;
	  justify-content: space-between;
	  background-color: ` + color + `;
	  outline: none;
	  border: 2px solid #ffd8d5;
	  font-size: 1.3rem;
	  color: #ff847c;
	  padding: 1rem 1rem;
	  cursor: pointer;
	  position: relative; /* esta es muy importante probablemente*/
	  overflow: hidden;  /* ESTA DEBE SER Importante . si se
descomenta se puede observar el secreto de algunas animaciones */
	  width: 175px;
	  margin-bottom: 1rem;
	}	 `;  
 }

restaurar() {
	this.divInterno.textContent = this.textoInicial;
	this.veces = this.vecesIniciales;
	clearInterval(this.intervalo);
}

 avanzarClick(){
	this.veces--;

	if (this.veces <=0) {
		this.divInterno.textContent = this.textoFinal;
		
		this.intervalo = setInterval( function (objeto)
		 { 
			return function(evento) 
			{
				objeto.restaurar(evento);
			}
		} (this) , this.tiempo);
	}
}
  constructor() {
    // Always call super first in constructor
    super();

    // Create a shadow root
   this.veces = this.getAttribute('veces');
	this.vecesIniciales = this.veces;   
	this.color = this.getAttribute('color');
   this.tiempo = this.getAttribute('tiempo');
   this.divs = this.getElementsByTagName("div");

   
   this.estilo = document.createElement('style');
   this.estilo.textContent = this.creaEstiloBoton(this.color);
   console.log(this.estilo.textContent);
   
  	this.shadow = this.attachShadow({mode: 'open'});
    //this.shadow = this;



	this.textoInicial = this.divs[0].textContent;
	this.textoFinal  = this.divs[1].textContent;

	this.divInterno = document.createElement("div");
	this.divInterno.textContent = this.textoInicial;
    this.divInterno.setAttribute("class","boton-base");
    // Attach the created elements to the shadow dom
    this.shadow.appendChild(this.estilo);
	this.shadow.appendChild(this.divInterno);
    

    this.addEventListener("click", function (objeto)
	 { 
		return function(evento) 
			{
			objeto.avanzarClick(evento);
			}
	}
	(this) );
	  
	console.log(" casi inicializado");
    
  
	console.log("inicializado");
  }// constructor

} // clase


let cont = 0;

  function inicializar(){
	console.log("hola");
	botonTembloroso = document.getElementById("botonTembloroso");
	botonSeleccionar = document.getElementById("botonSeleccionar");
         
    botonSeleccionar.addEventListener('animationend', (event) => {
		cont++;
		console.log(cont);
		if (cont == 3 ){
			botonTembloroso.style.animationName = "animacionFinal";				
			console.log("cmabiando");
				}
            } );
   
	customElements.define("mi-boton",MiBoton);
     
   }

