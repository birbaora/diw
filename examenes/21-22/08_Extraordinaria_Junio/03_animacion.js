let distancia = 100;
let divBox;
let permitir = true;

function establecerDistancia(){
	let elementoRoot= document.documentElement;
 	let stringDistancia = ""+ distancia+"%";
	elementoRoot.style.setProperty("--distancia",stringDistancia)

}

function masDistancia(){
	if (!permitir ) return;
	distancia = distancia * 1.1;
	establecerDistancia();
	permitir=false;
}

function menosDistancia(){
	if (!permitir ) return;
	distancia = distancia / 1.1;
	establecerDistancia();
	permitir = false;
}



function inicializar(){

	divBox = document.getElementById("caja");
	
	divBox.addEventListener("animationstart",()=>{permitir = true;});

	botonMas = document.getElementById("mas");
	botonMas.addEventListener("click",masDistancia);
	
	botonMenos = document.getElementById("menos");
	botonMenos.addEventListener("click",menosDistancia);
}

document.addEventListener("DOMContentLoaded", inicializar);